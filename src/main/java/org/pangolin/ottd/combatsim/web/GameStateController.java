package org.pangolin.ottd.combatsim.web;

import org.pangolin.ottd.combatsim.data.Combat;
import org.pangolin.ottd.combatsim.data.CombatGroup;
import org.pangolin.ottd.combatsim.service.GameStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/state")
public class GameStateController
{
    @Autowired
    GameStateService gameStateService;

    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    public List<CombatGroup> currentGameState ()
    {
        return gameStateService.getGameState().groups;
    }

    @RequestMapping(value = "/combats", method = RequestMethod.GET)
    public List<Combat> currentFights ()
    {
        return gameStateService.getGameState().activeCombats;
    }

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public void reset()
    {
        gameStateService.initGameState();
    }
}

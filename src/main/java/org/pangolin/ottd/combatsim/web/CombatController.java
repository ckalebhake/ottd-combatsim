package org.pangolin.ottd.combatsim.web;

import org.pangolin.ottd.combatsim.data.Activity;
import org.pangolin.ottd.combatsim.data.Combat;
import org.pangolin.ottd.combatsim.data.CombatGroup;
import org.pangolin.ottd.combatsim.exception.CombatNotFoundException;
import org.pangolin.ottd.combatsim.exception.GroupNotFoundException;
import org.pangolin.ottd.combatsim.service.CombatResolutionService;
import org.pangolin.ottd.combatsim.service.GameStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/combat")
public class CombatController
{
    private int nextCombatId;

    @Autowired
    private GameStateService gameStateService;

    @Autowired
    private CombatResolutionService combatResolutionService;

    public CombatController()
    {
        nextCombatId = 1;
    }

    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public ResponseEntity<String> startFight(
            @RequestParam("A") String groupNameA,
            @RequestParam("B") String groupNameB)
    {
        //fetch groups that will be fighting
        CombatGroup groupA;
        CombatGroup groupB;
        try
        {
            groupA = gameStateService.findGroup(groupNameA);
            groupB = gameStateService.findGroup(groupNameB);
        } catch (GroupNotFoundException e)
        {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No group exists with the name " + e.getMessage());
        }

        //validation
        if (groupA.getCurrentActivity() == Activity.COMBAT ||
            groupB.getCurrentActivity() == Activity.COMBAT)
        {
            return ResponseEntity.badRequest().body("one or both groups is already engaged in combat");
        }

        //initiate the fight
        Combat newFight = new Combat(groupA, groupB, nextCombatId++);
        groupA.setCurrentActivity(Activity.COMBAT);
        groupB.setCurrentActivity(Activity.COMBAT);
        gameStateService.getGameState().activeCombats.add(newFight);

        //log it
        Logger combatLogger = Logger.getLogger("Combat");
        combatLogger.info("New Combat: " + groupA.getName() + "/" + groupB.getName() + ".  curPower/MaxPower/Fatigue:");
        combatLogger.info(groupA.getName() + ": " + String.valueOf(groupA.getCurPowerRating()) + "/" + String.valueOf(groupA.getMaxPowerRating()) + "/" + String.valueOf(groupA.getCurFatigue()));
        combatLogger.info(groupB.getName() + ": " + String.valueOf(groupB.getCurPowerRating()) + "/" + String.valueOf(groupB.getMaxPowerRating()) + "/" + String.valueOf(groupB.getCurFatigue()));

        //return the ID of the new conflict
        return ResponseEntity.ok(String.valueOf(newFight.getCombatID()));
    }

    @RequestMapping(value = "/advance", method = RequestMethod.POST)
    public ResponseEntity advanceCombat(@RequestParam("id") int combatId)
    {
        //retrieve the combat to advance
        Combat combat;
        try
        {
            combat = gameStateService.findCombat(combatId);
        } catch (CombatNotFoundException e)
        {
            return ResponseEntity.badRequest().body("Could not find combat with id " + e.getMessage());
        }

        //advance combat
        combatResolutionService.advance(combat);

        //return it
        return ResponseEntity.ok(combat);
    }

    @RequestMapping(value = "/resolve", method = RequestMethod.POST)
    public ResponseEntity resolveCombat(@RequestParam("id") int combatId)
    {
        //retrieve the combat to resolve
        Combat combat;
        try
        {
            combat = gameStateService.findCombat(combatId);
        } catch (CombatNotFoundException e)
        {
            return ResponseEntity.badRequest().body("Could not find combat with id " + e.getMessage());
        }

        //advance combat until it is complete
        while (!combat.isComplete())
            combatResolutionService.advance(combat);

        //return it
        return ResponseEntity.ok(combat);
    }

    @RequestMapping(value = "/advanceAll", method = RequestMethod.POST)
    public List<Combat> advanceAllCombat()
    {
        List<Combat> results = new ArrayList<>();

        for (Combat c : gameStateService.getGameState().activeCombats)
        {
            combatResolutionService.advance(c);
            results.add(c);
        }

        return results;
    }

    @RequestMapping(value = "/resolveall", method = RequestMethod.POST)
    public List<Combat> resolveAllCombat()
    {
        List<Combat> results = new ArrayList<>();

        for (Combat c : gameStateService.getGameState().activeCombats)
        {
            while (!c.isComplete())
                combatResolutionService.advance(c);

            results.add(c);
        }

        return results;
    }
}

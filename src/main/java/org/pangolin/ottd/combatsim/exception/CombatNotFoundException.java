package org.pangolin.ottd.combatsim.exception;

public class CombatNotFoundException extends IllegalArgumentException
{
    public CombatNotFoundException(String s)
    {
        super(s);
    }
    public CombatNotFoundException(int id) {super(String.valueOf(id));}
}

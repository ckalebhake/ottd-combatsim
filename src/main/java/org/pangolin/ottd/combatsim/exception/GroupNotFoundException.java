package org.pangolin.ottd.combatsim.exception;

public class GroupNotFoundException extends IllegalArgumentException
{
    public GroupNotFoundException(String s)
    {
        super(s);
    }
}

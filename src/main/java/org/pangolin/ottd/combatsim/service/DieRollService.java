package org.pangolin.ottd.combatsim.service;

import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.logging.Logger;

@Service
public class DieRollService
{
    private Random random;

    /**
     * performs and logs a die roll
     * @param numDice number of dice to roll
     * @param sides number of sides on those dice
     * @param modifier modifier to add after the roll
     * @param label how to label this roll in the logs
     * @return the roll total
     */
    public int roll(int numDice, int sides, int modifier, String label)
    {
        if (numDice < 1)
            throw new IllegalArgumentException("must roll at least one die");

        if (sides < 2)
            throw new IllegalArgumentException("die must have at least two sides");

        //repeatedly roll dice and add up the total
        int result = 0;
        for (int i = 0; i < numDice; i++)
            result += random.nextInt(sides - 1) + 1;

        int total = result + modifier;

        //log it
        Logger.getLogger("Roll").fine(label
                + String.valueOf(numDice) + "d" + String.valueOf(sides) + (modifier == 0? "" : "+" + String.valueOf(modifier)) + " = "
                + String.valueOf(result) + (modifier == 0? "":"+" + String.valueOf(modifier)) + " = "
                + String.valueOf(total)
        );

        return total;
    }

    /**
     * performs and logs a die roll with no label
     * @param numDice number of dice to roll
     * @param sides number of sides on those dice
     * @param modifier modifier to add after the roll
     * @return the roll total
     */
    public int roll(int numDice, int sides, int modifier) { return roll(numDice, sides, modifier, ""); }

    /**
     * performs and logs a die roll with no label or modifier
     * @param numDice number of dice to roll
     * @param sides number of sides on those dice
     * @return the roll total
     */
    public int roll(int numDice, int sides) { return roll(numDice, sides, 0, ""); }

    public DieRollService()
    {
        random = new Random();
    }
}

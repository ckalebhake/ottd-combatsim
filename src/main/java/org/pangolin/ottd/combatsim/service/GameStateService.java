package org.pangolin.ottd.combatsim.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.pangolin.ottd.combatsim.data.*;
import org.pangolin.ottd.combatsim.exception.CombatNotFoundException;
import org.pangolin.ottd.combatsim.exception.GroupNotFoundException;
import org.pangolin.ottd.combatsim.exception.InvalidConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

@Service
public class GameStateService
{
    //stores the configured game state
    private JsonConfig jsonConfig;

    //stores the current game state
    private GameState gameState;

    @Autowired
    private ObjectMapper jsonMapper;

    @Value("${OttD.CombatSim.configFileLocation}")
    private String configFileLocation;

    //performs initial loading of configuration data
    @PostConstruct
    public void loadConfigFile() throws IOException
    {
        //locate the config file
        File configFile = getConfigFile();

        if(!configFile.exists())
            throw new FileNotFoundException("Could not find the config file (" + configFileLocation + ")");

        Logger.getLogger("JsonConfigService").info("attempting to load config file (" + configFileLocation + ")");

        try
        {
            jsonConfig = jsonMapper.readValue(configFile, new TypeReference<JsonConfig>() {}); //load the file
            validateConfiguration();
        } catch (Exception e)
        {
            throw new InvalidConfigurationException("Exception occurred parsing the config file", e);
        }

        //set up for the first game
        initGameState();
    }

    /**
     * re-initializes the game state
     */
    public void initGameState()
    {
        gameState = new GameState();
        for(CombatGroup configuredGroup : jsonConfig.getGroups())
        {
            CombatGroup newGroup = new CombatGroup();
            newGroup.setName(configuredGroup.getName());
            newGroup.setCaster(configuredGroup.getCaster() == null ? false: configuredGroup.getCaster()); //default to false if not specified
            newGroup.setMaxPowerRating(configuredGroup.getMaxPowerRating());
            newGroup.setCurPowerRating(configuredGroup.getMaxPowerRating());
            newGroup.setCurFatigue(0);
            newGroup.setCurrentActivity(Activity.IDLE);
            gameState.groups.add(newGroup);
        }
    }

    //locates and returns the config file
    @org.jetbrains.annotations.NotNull
    @org.jetbrains.annotations.Contract(" -> new")
    private File getConfigFile()
    {
        //if there is a JVM property set for the config file location, use that instead
        String JVMPropertyValue = System.getProperty("OttD.CombatSim.configFileLocation");
        if(JVMPropertyValue != null && !JVMPropertyValue.isEmpty())
            configFileLocation = JVMPropertyValue;

        //first attempt to load the config as a resource
        ClassLoader classLoader = getClass().getClassLoader();

        //try-catch here because some classloaders throw an IllegalArgumentException if you pass them an absolute path
        URL resource = null;
        try
        {
            resource = classLoader.getResource(configFileLocation);
        } catch (IllegalArgumentException ignored) {}

        if (resource != null)
        {
            return new File(resource.getFile().replace("%20", " ")); //the replace() call here is to handle paths with spaces
        }
        else
        {
            //we failed to find it as a resource, so try a direct file lookup instead
            return new File(configFileLocation);
        }
    }

    //confirms the config file is valid
    private void validateConfiguration()
    {
        //used to test for duplicate names
        Set<String> groupNames = new HashSet<>();

        for (CombatGroup group : jsonConfig.getGroups())
        {
            if (group.getName() == null)
                throw new InvalidConfigurationException("A combat group is missing a name");

            if (!groupNames.add(group.getName()))
                throw new InvalidConfigurationException("Multiple groups with the name \"" + group.getName() + "\"");

            if(group.getMaxPowerRating() == null || group.getMaxPowerRating() <= 0)
                throw new InvalidConfigurationException("The group " + group.getName() + " must have a positive maxPowerRating");
        }
    }

    public GameState getGameState()
    {
        return gameState;
    }

    /**
     * retrieves a CombatGroup from the current game state
     * @param groupName name of the group to retrieve
     * @return the requested group
     * @throws org.pangolin.ottd.combatsim.exception.GroupNotFoundException if the group does not exist
     */
    public CombatGroup findGroup(String groupName)
    {
        for (CombatGroup group : gameState.groups)
            if(group.getName().equalsIgnoreCase(groupName))
                return group;

        throw new GroupNotFoundException(groupName);
    }

    public Combat findCombat(int combatId)
    {
        for (Combat combat : gameState.activeCombats)
            if(combat.getCombatID() == combatId)
                return combat;

        throw new CombatNotFoundException(combatId);
    }
}

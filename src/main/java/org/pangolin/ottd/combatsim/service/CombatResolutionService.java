package org.pangolin.ottd.combatsim.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.pangolin.ottd.combatsim.data.Activity;
import org.pangolin.ottd.combatsim.data.Combat;
import org.pangolin.ottd.combatsim.data.CombatGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class CombatResolutionService
{
    @Autowired
    @JsonIgnore
    private DieRollService dieRollService;

    @Autowired
    private GameStateService gameStateService;

    /**
     * resolves a single round of combat.  For more information, see https://www.worldanvil.com/w/prima-that-murderous-penguin-who-secretly-lives-in-your-sock-drawer/a/vigrid---offscreen-combat-resolution-rules-article
     */
    public void advance(Combat combat)
    {
        CombatGroup groupA = combat.getGroupA();
        CombatGroup groupB = combat.getGroupB();

        //increment round counter
        combat.incrementDuration();

        Logger combatLogger = Logger.getLogger("Combat");
        combatLogger.fine("start of round " + combat.getDuration() + ": " + groupA.getName() + " vs. " + groupB.getName());

        //perform combat rolls
        int groupACombatRoll = dieRollService.roll(2,10, groupA.getCurPowerRating(), "combat roll (" + groupA.getName() + "): ");
        int groupBCombatRoll = dieRollService.roll(2,10, groupB.getCurPowerRating(), "combat roll (" + groupB.getName() + "): ");

        //perform damage rolls
        int groupADamageRoll;
        if (groupACombatRoll >= (groupBCombatRoll+10))
        {
            //group A: large damage bonus
            groupADamageRoll = dieRollService.roll(2,6,10,"damage roll (" + groupA.getName() + ")");
        }
        else if (groupACombatRoll >= groupBCombatRoll+5)
        {
            //group A: damage bonus
            groupADamageRoll = dieRollService.roll(1,6,5,"damage roll (" + groupA.getName() + ")");
        }
        else
        {
            //group A: normal damage
            groupADamageRoll = dieRollService.roll(1,6,0,"damage roll (" + groupA.getName() + ")");
        }

        int groupBDamageRoll;
        if (groupBCombatRoll >= (groupBCombatRoll+10))
        {
            //group B: large damage bonus
            groupBDamageRoll = dieRollService.roll(2,6,10,"damage roll (" + groupB.getName() + ")");
        }
        else if (groupBCombatRoll >= groupBCombatRoll+5)
        {
            //group B: damage bonus
            groupBDamageRoll = dieRollService.roll(1,6,5,"damage roll (" + groupB.getName() + ")");
        }
        else
        {
            //group B: normal damage
            groupBDamageRoll = dieRollService.roll(1,6,0,"damage roll (" + groupB.getName() + ")");
        }

        //update power ratings
        groupA.setCurPowerRating(groupA.getCurPowerRating() - groupBDamageRoll);
        groupB.setCurPowerRating(groupB.getCurPowerRating() - groupADamageRoll);

        //log change
        combatLogger.info("end of round " + combat.getDuration() + ": " + groupA.getName() + " vs. " + groupB.getName() + ": "
                + groupA.getCurPowerRating() + "/" + groupB.getCurPowerRating());

        //check for combat completion and perform cleanup if necessary
        if (groupA.getCurPowerRating() <= 0 ||
                groupB.getCurPowerRating() <= 0)
        {
            //fight is done
            combat.setComplete(true);
            groupA.setCurrentActivity(Activity.IDLE);
            groupB.setCurrentActivity(Activity.IDLE);

            //calculate fatigue.  Java automatically rounds towards 0 here since both operands are int
            int groupAPowerLoss = combat.getGroupAOriginalPower() - groupA.getCurPowerRating();
            if (groupA.getCaster())
                groupA.setCurFatigue(groupA.getCurFatigue() + (groupAPowerLoss / 2));
            else
                groupA.setCurFatigue(groupA.getCurFatigue() + (groupAPowerLoss / 4));

            int groupBPowerLoss = combat.getGroupAOriginalPower() - groupB.getCurPowerRating();
            if (groupB.getCaster())
                groupB.setCurFatigue(groupB.getCurFatigue() + (groupBPowerLoss / 2));
            else
                groupB.setCurFatigue(groupB.getCurFatigue() + (groupBPowerLoss / 4));

            //remove this from the list of active fights
            gameStateService.getGameState().activeCombats.remove(combat);

            //log it
            combatLogger.info("Combat complete after " + combat.getDuration() + " rounds.  curPower/MaxPower/Fatigue:");
            combatLogger.info(groupA.getName() + ": " + String.valueOf(groupA.getCurPowerRating()) + "/" + String.valueOf(groupA.getMaxPowerRating()) + "/" + String.valueOf(groupA.getCurFatigue()));
            combatLogger.info(groupB.getName() + ": " + String.valueOf(groupB.getCurPowerRating()) + "/" + String.valueOf(groupB.getMaxPowerRating()) + "/" + String.valueOf(groupB.getCurFatigue()));
        }
    }
}

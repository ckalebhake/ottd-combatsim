package org.pangolin.ottd.combatsim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CombatsimApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(CombatsimApplication.class, args);
    }

}

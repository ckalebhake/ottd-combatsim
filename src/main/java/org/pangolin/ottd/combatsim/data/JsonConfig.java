package org.pangolin.ottd.combatsim.data;

import java.util.List;

/**
 * represents the contents of the config file
 */
public class JsonConfig
{
    private List<CombatGroup> groups;

    public List<CombatGroup> getGroups()
    {
        return groups;
    }

    public void setGroups(List<CombatGroup> groups)
    {
        this.groups = groups;
    }
}

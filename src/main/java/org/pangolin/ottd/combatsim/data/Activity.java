package org.pangolin.ottd.combatsim.data;

/**
 * The various activities a team could currently be engaged in
 */
public enum Activity
{
    IDLE,
    COMBAT,
    SHORT_REST,
    LONG_REST,
}

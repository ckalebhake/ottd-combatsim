package org.pangolin.ottd.combatsim.data;

import java.util.ArrayList;
import java.util.List;

/**
 * represents current state of a game in progress
 */
public class GameState
{
    public List<CombatGroup> groups;
    public List<Combat> activeCombats;

    public GameState()
    {
        this.groups = new ArrayList<>();
        this.activeCombats = new ArrayList<>();
    }
}

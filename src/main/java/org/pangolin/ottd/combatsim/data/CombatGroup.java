package org.pangolin.ottd.combatsim.data;

/**
 * represents a group of combatants
 */
public class CombatGroup
{
    private String name;
    private Integer maxPowerRating;
    private Integer curPowerRating;
    private Integer curFatigue;
    private Boolean isCaster;
    private Activity currentActivity;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getMaxPowerRating()
    {
        return maxPowerRating;
    }

    public void setMaxPowerRating(Integer maxPowerRating)
    {
        this.maxPowerRating = maxPowerRating;
    }

    public Integer getCurPowerRating()
    {
        return curPowerRating;
    }

    public void setCurPowerRating(Integer curPowerRating)
    {
        this.curPowerRating = curPowerRating;
    }

    public Integer getCurFatigue()
    {
        return curFatigue;
    }

    public void setCurFatigue(Integer curFatigue)
    {
        this.curFatigue = curFatigue;
    }

    public Boolean getCaster()
    {
        return isCaster;
    }

    public void setCaster(Boolean caster)
    {
        isCaster = caster;
    }

    public Activity getCurrentActivity()
    {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity)
    {
        this.currentActivity = currentActivity;
    }
}

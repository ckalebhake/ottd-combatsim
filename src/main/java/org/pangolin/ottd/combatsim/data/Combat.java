package org.pangolin.ottd.combatsim.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * represents a fight in progress
 */
public class Combat
{
    private CombatGroup groupA;
    private CombatGroup groupB;
    @JsonIgnore private int groupAOriginalPower;
    @JsonIgnore private int groupBOriginalPower;
    private int combatID;
    private int duration;
    private boolean complete;

    public Combat(CombatGroup A, CombatGroup B, int id)
    {
        groupA = A;
        groupB = B;
        groupAOriginalPower = groupA.getCurPowerRating();
        groupBOriginalPower = groupB.getCurPowerRating();
        combatID = id;
        duration = 0;
        complete = false;
    }

    public CombatGroup getGroupA()
    {
        return groupA;
    }

    public CombatGroup getGroupB()
    {
        return groupB;
    }

    public int getCombatID()
    {
        return combatID;
    }

    public boolean isComplete()
    {
        return complete;
    }

    public void setComplete(boolean b)
    {
        complete = b;
    }

    public int getGroupAOriginalPower()
    {
        return groupAOriginalPower;
    }

    public int getGroupBOriginalPower()
    {
        return groupBOriginalPower;
    }

    public int getDuration()
    {
        return duration;
    }

    public void incrementDuration()
    {
        this.duration+=1;
    }
}
